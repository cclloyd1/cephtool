#!/bin/bash

cleanup ()
{
  kill -s SIGTERM $!
  echo "SIGTERM activated.  Exiting application..."
  exit 0
}

trap cleanup SIGINT SIGTERM

while true; do
  if ! pg_isready -h "$DB_HOST" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" -t 2
  then
    echo "Cannot connect to database... retrying in 5 seconds"
	  sleep 5 & wait $!
	else
	  echo 'Starting app...'
    cd /app/api || exit;
    python manage.py migrate || exit;
    uwsgi --show-config --attach-daemon=nginx & wait $!
	fi;
done


