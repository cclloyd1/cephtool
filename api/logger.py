import logging
import coloredlogs

from cephtool.settings import DEBUG

class MultilineFormatter(logging.Formatter):
    def format(self, record: logging.LogRecord):
        save_msg = record.msg
        output = ""
        for line in save_msg.splitlines():
            record.msg = line
            output += super().format(record) + "\n"
        record.msg = save_msg
        record.message = output
        return output


def get_logger(name='shittywizard', level=logging.INFO):
    logger = logging.getLogger(name)

    if DEBUG:
        level = logging.DEBUG

    logger.setLevel(level)
    # formatter = MultilineFormatter('[%(levelname)8s] %(message)s')
    coloredlogs.install(level=level, logger=logger, fmt='[%(levelname)8s] %(message)s')
    return logger


log = get_logger(level=logging.INFO)
