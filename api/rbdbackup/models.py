from django.db import models
from django.utils import timezone
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import User

import datetime

class RBDBackup(models.Model):
    objects = models.Manager()

    name = models.CharField('Backup Name', max_length=256, default='kubernetes-pvc')
    namespace = models.CharField('Namespace', max_length=64, default='default')
    pvc = models.CharField('Persistent Volume Claim', max_length=256, default='kubernetes-pvc')
    pv = models.CharField('Persistent Volume', max_length=256, default='kubernetes-pv')

    created = models.DateTimeField('created_at', default=timezone.now, editable=False)
    modified = models.DateTimeField('modified_at', default=timezone.now, editable=False)
    deleted = models.DateTimeField('deleted_at', default=None, null=True, blank=True)

    #def __str__(self):
    #    return '%s' % self.Name

