from rest_framework import routers
from .viewsets import RBDBackupViewset

router = routers.SimpleRouter()

router.register(r'^api/backups', RBDBackupViewset, 'RBDBackups')
