from rest_framework import viewsets, mixins, status
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied

from .serializers import RBDBackupSerializer
from .models import RBDBackup


class RBDBackupViewset(
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
    ):

    queryset = RBDBackup.objects.all()
    serializer_class = RBDBackupSerializer

    '''
    def destroy(self, request, *args, **kwargs):
        user = request.user
        instance = self.get_object()

        if user.is_anonymous:
            raise PermissionDenied("You must be logged in to delete dash groups")

        if instance.owner.id != user.id and not user.is_staff:
            raise PermissionDenied("You are not the owner of this group.")

        # Remove group ID from list of RBDBackups in dashboard
        dashboard = instance.dashboard
        dashboard.RBDBackups.remove(instance.id)
        dashboard.save()

        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    @action(methods=['get'], detail=False)
    def mine(self, request):
        if request.user is None:
            raise PermissionDenied("You must be logged in to retrieve dash groups.")

        queryset = RBDBackup.objects.filter(owner=request.user.id)
        page = self.paginate_queryset(queryset)
        if page is not None:
            print('page not none')
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        print('page is none')
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
    
    @action(methods=['get'], detail=False)
    def minedetails(self, request):
        if request.user is None:
            raise PermissionDenied("You must be logged in to retrieve dash groups.")

        queryset = RBDBackup.objects.filter(owner=request.user.id).order_by('-created')
        #page = self.paginate_queryset(queryset)
        print('page is none')
        serializer = self.get_serializer(queryset, many=True)
        #print(serializer.data)
        RBDBackups = []
        for group in serializer.data:
            print(group['dashTiles'])
            tiles = Dashtile.objects.filter(RBDBackup=group['id'])
            tile_serializer = DashtileSerializer(tiles, many=True)
            print(tile_serializer.data)
            group['dashTiles'] = tile_serializer.data
            RBDBackups.append(group)

        return Response(RBDBackups)

    @action(methods=['get'], detail=True)
    def tiles(self, request, pk=None):
        RBDBackup = self.get_object()

        if request.user is None:
            raise PermissionDenied("You must be logged in to retrieve dash tiles.")

        tiles = Dashtile.objects.filter(RBDBackup=RBDBackup.id)
        tile_serializer = DashtileSerializer(tiles, many=True)
        return Response(tile_serializer.data)
    '''

