from django.apps import AppConfig


class RBDBackupConfig(AppConfig):
    name = 'rbdbackup'
    label = 'rbdbackup'
    verbose_name = 'RBD Backup'
