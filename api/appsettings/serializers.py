from rest_framework import views, serializers, status
from rest_framework.exceptions import PermissionDenied, ValidationError
from rest_framework.response import Response

from .models import AppSettings


class AppSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppSettings
        fields = '__all__'

    '''
    def create(self, validated_data):
        request = self.context.get("request")
        user = request.user
        dashboard = validated_data['dashboard']

        if user.is_anonymous:
            raise PermissionDenied("You must be logged in to create this object")

        if dashboard.owner.id != user.id or not user.is_staff:
            raise PermissionDenied("You do not own the dashboard you are trying to add this group to.")

        validated_data['owner'] = user
        new_group = RBDBackup.objects.create(**validated_data)
        dashboard.RBDBackups.append(new_group.id)
        dashboard.save()
        return new_group

    def update(self, instance, validated_data):
        request = self.context.get("request")
        user = request.user

        if user.is_anonymous:
            raise PermissionDenied("You must be logged in to update dash groups.")

        if user.id != instance.owner.id and not user.is_staff:
            raise PermissionDenied("You don't have permission to change this dash group.")

        # TODO: Remove?  Tile now removed from list when deleted.
        # Remove tiles that don't exist from list
        for tile in validated_data['dashTiles']:
            exists = Dashtile.objects.filter(id=tile).first()
            if not exists:
                validated_data['dashTiles'].remove(tile)

        [setattr(instance, k, v) for k, v in validated_data.items()]
        instance.save()
        return instance
    '''
