from django.apps import AppConfig


class AppSettingsConfig(AppConfig):
    name = 'appsettings'
    label = 'appsettings'
    verbose_name = 'App Settings'
