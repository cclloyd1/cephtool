from rest_framework import viewsets, mixins, status
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied

from .serializers import AppSettingsSerializer
from .models import AppSettings


class AppSettingsViewset(
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
    ):

    queryset = AppSettings.objects.all()
    serializer_class = AppSettingsSerializer

    @action(methods=['get'], detail=False)
    def get(self, request):
        if request.user.is_anonymous:
            raise PermissionDenied('You must be logged in to retrieve app settings.')

        settings = AppSettings.objects.first()
        serializer = self.get_serializer(settings, many=False)
        return Response(serializer.data)

