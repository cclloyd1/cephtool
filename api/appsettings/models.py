from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import User


class AppSettings(models.Model):
    objects = models.Manager()

    numBackups = models.IntegerField('Number of Backups', default=3, help_text='Default number of backups to iterate automatically. Default: 3')
    maxBackups = models.IntegerField('Max Backups', default=10, help_text='Max number of backups to keep (manual backups not automatically removed).  Default: 10')
    retainDeleted = models.BooleanField('Retain Deleted Backups', default=True, help_text='Retain backups from deleted PVCs, Default: True')
    retainDays = models.IntegerField('Retain Deleted Days', default=60, help_text='Days to retail backups from deleted PVCs, in days.  If retainDeleted==True, backups will be deleted after X days.  Default: 60 days')
    registrationEnabled = models.BooleanField('Registration Enabled', default=True, help_text='If you disable registration, the only way to create accounts is through the admin interface.  Recommended to disable registration if you\'re using external authentication such as LDAP')
    liveNotifications = models.BooleanField('Live Notifications', default=True, help_text='Get live progress on the backups.  Requires a Pusher.com account.  Default: True')
    memoryRead = models.BigIntegerField('Read/Write Chunk Size', default=128, help_text='Chunk size when reading/writing files in MiB.  Default: 128 MiB')  # 128 MiB in Bytes

    def save(self, *args, **kwargs):
        if not self.pk and AppSettings.objects.exists():
            raise ValidationError('There is can be only one AppSettings instance')
        return super(AppSettings, self).save(*args, **kwargs)

