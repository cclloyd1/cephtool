from rest_framework import routers
from .viewsets import AppSettingsViewset

router = routers.SimpleRouter()

router.register(r'^api/settings', AppSettingsViewset, 'AppSettings')
