from kubernetes import client, config
from kubernetes.client.rest import ApiException
from pprint import pprint
import os
import json
from collections import namedtuple
import base64
import platform
import errno

LINUX = False
if platform.system() == 'Linux':
    import rados
    import rbd
    LINUX = True


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python ≥ 2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

KUBECONF = os.getenv('KUBECONF', '~/.kube/config')
PROVISIONER = os.getenv('PROVISIONER', 'ceph.com/rbd')
STORAGE_CLASSES = os.getenv('STORAGE_CLASSES', 'slipspace').split(',')

kubecfg = config.load_kube_config(KUBECONF)
apiClient = client.ApiClient(kubecfg)
v1 = client.CoreV1Api(apiClient)
v1_storage = client.StorageV1Api(apiClient)


ret = v1.list_namespace()
namespaces = []
for ns in ret.items:
    namespaces.append(ns.metadata.name)

'''
storage_classes = []
ret = v1_storage.list_storage_class()
for item in ret.items:
    if item.provisioner == PROVISIONER:
        for storageclass in STORAGE_CLASSES:
            if item.metadata.name == storageclass:
                new = item
                new.metadata.annotations = None
                #new.parameters = json.loads(item.parameters, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))

                storage_classes.append(new)

for storage in storage_classes:
    print(storage.metadata.name)
    print(storage.parameters['pool'])
    ceph_key_secret = v1.read_namespaced_secret(name=storage.parameters['userSecretName'], namespace=storage.parameters['userSecretNamespace'])
    ceph_key = base64.b64decode(ceph_key_secret.data['key']).decode('utf-8')
'''

if LINUX:
    # Ceph config and connect to cluster
    conf_path = '/etc/ceph/ceph.conf'
    key_path = '/etc/ceph/ceph.client.admin.keyring'
    cluster = rados.Rados(conffile=conf_path, conf=dict(keyring=key_path))
    cluster.connect()
    print(f'[INFO] == FSID: {cluster.get_fsid().decode("utf-8")}')
    pools = cluster.list_pools()
    print(f'[INFO] == Pools: {pools}')

backups = []
pvcs = []
pvs = []

for item in v1.list_persistent_volume_claim_for_all_namespaces().items:
    if item.spec.storage_class_name == 'slipspace':
        pvcs.append(item)

for pvc in pvcs:
    backup = dict()
    volume_name = pvc.spec.volume_name
    volume = v1.read_persistent_volume(volume_name)
    backup['name'] = pvc.metadata.name
    backup['namespace'] = pvc.metadata.namespace
    backup['storageclass'] = pvc.spec.storage_class_name
    backup['size'] = pvc.spec.resources.requests['storage']
    backup['pool'] = volume.spec.rbd.pool
    backup['image'] = volume.spec.rbd.image
    backup_dir = os.path.join('/srv/Ceph/K8s/', 'pools', backup['pool'], 'namespaces', backup['namespace'], 'pvcs', backup['name'])
    backup['backup_dir'] = backup_dir
    backups.append(backup)

for backup in backups:
    mkdir_p(backup['backup_dir'])
    cmd = f"rbd export-diff -p {backup['pool']} {backup['image']} {backup['backup_dir']}/{backup['image']}_size-{backup['size']}.img"
    print(cmd)
    #os.system(cmd)


'''
# import function (tested to work)
IMPORT = False
if IMPORT:
    #cmd = f"rbd create --size {backup['size']} -p {backup['pool']} {backup['image']}"
    cmd = f"rbd create --image-feature layering --size {backup['size']} -p {backup['pool']} test"
    os.system(cmd)

    cmd = f"rbd import-diff -p {backup['pool']} {backup['image']}_size-{backup['size']}.img {backup['image']}"
    os.system(cmd)
'''


