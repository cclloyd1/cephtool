import rados
import rbd
import sys
import os
from os import listdir
from os.path import isfile, join
import re

def get_files_in_directory(directory):
    files = [f for f in listdir(directory) if isfile(join(directory, f))]
    return files


BACKUP_DIR = os.path.abspath('/srv/Ceph/pools/')
if not os.path.exists(BACKUP_DIR):
    os.mkdir(BACKUP_DIR)

BACKUP_POOLS = ['Slipspace']
BACKUP_COUNT = 3

# Ceph config and connect to cluster
conf_path = '/etc/ceph/ceph.conf'
key_path = '/etc/ceph/ceph.client.admin.keyring'
cluster = rados.Rados(conffile=conf_path, conf=dict(keyring=key_path))
cluster.connect()
print(f'[INFO] == FSID: {cluster.get_fsid().decode("utf-8")}')

pools = cluster.list_pools()
print(f'[INFO] == Pools: {pools}')


class RBDImageBackup:
    def __init__(self, pool=None, name=None, size=0, files=[], backup_dir=None):
        self.pool = pool
        self.name = name
        self.size = size
        self.files = files
        self.backup_dir = backup_dir
        self.num_backups = 0

        if len(files) == 0:
            files_in_dir = get_files_in_directory(backup_dir)
            image_files = []
            for file in files_in_dir:
                if file.startswith(name):
                    image_files.append(file)
            self.files = image_files.copy()
            self.files.sort()
            self.num_backups = len(self.files)

    def delete_extra_backups(self):
        for file in self.files:
            pattern = '.+_size-\d+\.?(\d+)?.img'
            m = re.search(pattern, file)
            if m.group(1) is not None:
                spot = int(m.group(1))
                if spot >= BACKUP_COUNT:
                    os.remove(os.path.join(self.backup_dir, file))

    def iterate_backups(self, rollback=False):
        if len(self.files) == 0:
            return

        # Rename all files in directory
        pattern = '.+_size-\d+\.?(\d+)?.img'
        for file in reversed(self.files):
            m = re.search(pattern, file)
            if m.group(1) is not None:
                spot = int(m.group(1))
                if rollback:
                    spot = max(0, spot-1)
                else:
                    spot = spot + 1
                os.rename(os.path.join(backup_path, file), os.path.join(backup_path, f'{filename}.{spot}.img'))

        # Generate new list of files associated with image
        image_files = []
        files_in_dir = get_files_in_directory(self.backup_dir)
        for file in files_in_dir:
            if file.startswith(self.name):
                image_files.append(file)
        self.files = image_files.copy()

    def backup_image(self):
        dest_file = os.path.join(self.backup_dir, f'{self.name}_size-{self.size}.0.img.tmp')
        command = f'rbd export-diff {self.pool}/{self.name} {dest_file}'
        #print(command)
        code = os.system(command)
        #print(code)

        return code

    def move_backup(self):
        new_backup = os.path.join(self.backup_dir, f'{self.name}_size-{self.size}.0.img.tmp')
        os.rename(new_backup, new_backup[:-4])

    def __str__(self):
        header = '==RBD Backup:=='
        line1 = f'=={self.pool}/{self.name}\tSize: {self.size}'
        line2 = f'==BackupDir: {self.backup_dir}\tNum: {self.num_backups}'
        line3 = f'==Files: {self.files}'
        return f'{header}\n{line1}\n{line2}\n{line3}'


for pool in pools:
    if pool not in BACKUP_POOLS:
        continue

    # Create backup dir
    backup_path = os.path.join(BACKUP_DIR, pool)
    if not os.path.exists(backup_path):
        os.mkdir(backup_path)

    # Open pool and get images
    ioctx = cluster.open_ioctx(pool)
    rbd_inst = rbd.RBD()
    images = rbd_inst.list(ioctx)

    #
    for image_name in images:
        image = rbd.Image(ioctx, image_name)
        filename = f'{image_name}_size-{image.size()}'
        rbd_backup = RBDImageBackup(pool=pool, name=image_name, size=image.size(), backup_dir=backup_path)

        # Write script to scan for deleted volumes and move to deleted folder BUT DONT DELETE THE BACKUP
        #dest_file = os.path.join(rbd_backup.backup_dir, f'{rbd_backup.name}_size-{rbd_backup.size}.0.img.BAK')
        #command = f'rbd export-diff {rbd_backup.pool}/{rbd_backup.name} {dest_file}'
        #command = f'touch {dest_file}'
        # print(command)
        #code = os.system(command)

        # print(rbd_backup)
        rbd_backup.iterate_backups()
        success = rbd_backup.backup_image()
        if success == 0:
            print("[INFO] == Successfully backed up image.")
            rbd_backup.move_backup()
            rbd_backup.delete_extra_backups()

        else:
            print("[INFO] == Failed to backup image.")
            rbd_backup.iterate_backups(rollback=True)

        #exit(0)