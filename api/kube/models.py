from django.db import models
from django.utils import timezone
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import User

import datetime

#from dashboard.models import Dashboard


class RBDBackup(models.Model):
    #owner = models.ForeignKey(User, on_delete=models.SET_DEFAULT, default=1)
    #dashboard = models.IntegerField('dashboard', default=0)
    #dashboard = models.ForeignKey(Dashboard, on_delete=models.CASCADE)

    name = models.CharField('name', max_length=256, default='kubernetes-pvc')
    namespace = models.CharField('namespace', max_length=64, default='default')
    pvc = models.CharField('pvc', max_length=256, default='kubernetes-pvc')
    pv = models.CharField('pv', max_length=256, default='kubernetes-pv')
    #defaultCollapse = models.BooleanField('defaultCollapse', default=False)
    #dashTiles = ArrayField(models.IntegerField('dashTiles', default=0), default=list, blank=True)
    #dashTiles = ArrayField(models.IntegerField('dashTiles', default=0), default=list, blank=True)

    created = models.DateTimeField('created_at', default=timezone.now)
    modified = models.DateTimeField('modified_at', default=timezone.now)
    deleted = models.DateTimeField('deleted_at', default=None, null=True, blank=True)

    #def __str__(self):
    #    return '%s' % self.Name

