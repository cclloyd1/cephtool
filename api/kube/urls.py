from rest_framework.routers import SimpleRouter
from rest_framework_extensions.routers import ExtendedSimpleRouter
from .viewsets.kube import KubeViewset
from .viewsets.namespace import NamespaceViewset
from .viewsets.pvc import PVCViewset
from .viewsets.pv import PVViewset

router = ExtendedSimpleRouter()

'''
router.register(r'^api/kube', KubeViewset, 'kube')
router.register(r'^api/kube/namespace', NamespaceViewset, 'kube-ns')
'''
#router.register(r'^api/kube', KubeViewset, 'kube')

(
    router.register(r'^api/kube/namespace', NamespaceViewset, basename='ns')
          .register(r'pvc', PVCViewset, basename='pvc', parents_query_lookups=['ns'])
          .register(r'pv', PVViewset, basename='pv', parents_query_lookups=['ns', 'pvc'])
)
#router.register(r'api/kubenamespace', NamespaceViewset, 'kube-ns', parents_query_lookups=['kube_ns'])

