import os

from django.contrib.auth.models import User
from rest_framework import viewsets, mixins, status
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework_extensions import mixins as emixins

from cephtool.serializers import UserSerializer

from kubernetes import client, config
from kubernetes.client.rest import ApiException
from rest_framework_extensions.mixins import NestedViewSetMixin
import pusher


class PVViewset(
    NestedViewSetMixin,
    viewsets.ViewSet,
    mixins.RetrieveModelMixin,
):

    KUBECONF = os.getenv('KUBECONF', '~/.kube/config')
    PROVISIONER = os.getenv('PROVISIONER', 'ceph.com/rbd')
    STORAGE_CLASSES = os.getenv('STORAGE_CLASSES', 'slipspace').split(',')
    kubecfg = config.load_kube_config(KUBECONF)
    apiClient = client.ApiClient(kubecfg)
    v1 = client.CoreV1Api(apiClient)
    v1_storage = client.StorageV1Api(apiClient)

    pusher_client = pusher.Pusher(
        app_id='936225',
        key='3e0d0f36638d58049acf',
        secret='ce4cd669ca5230446b72',
        cluster='us2',
        ssl=True
    )

    def sanitize(self, resource):
        return self.apiClient.sanitize_for_serialization(resource)

    def list(self, request, *args, **kwargs):
        ns = kwargs['parent_lookup_ns']
        pvc = kwargs['parent_lookup_pvc']
        pvc_item = self.v1.read_namespaced_persistent_volume_claim(pvc, ns)
        ret = self.v1.read_persistent_volume(pvc_item.spec.volume_name)
        return Response(self.sanitize(ret))

    @action(methods=['get'], detail=False)
    def rbd(self, request, *args, **kwargs):
        ns = kwargs['parent_lookup_ns']
        pvc = kwargs['parent_lookup_pvc']
        pvc_item = self.v1.read_namespaced_persistent_volume_claim(pvc, ns)
        ret = self.v1.read_persistent_volume(pvc_item.spec.volume_name)
        return Response(self.sanitize(ret.spec.rbd))

    @action(methods=['get'], detail=False)
    def backup(self, request, *args, **kwargs):
        ns = kwargs['parent_lookup_ns']
        pvc = kwargs['parent_lookup_pvc']
        pvc_item = self.v1.read_namespaced_persistent_volume_claim(pvc, ns)
        ret = self.v1.read_persistent_volume(pvc_item.spec.volume_name)

        self.pusher_client.trigger(pvc_item.metadata.uid, 'progress', {'message': 'hello world'})

        return Response(self.sanitize(ret.spec.rbd))



