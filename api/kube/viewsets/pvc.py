import os
import random

from django.contrib.auth.models import User
from rest_framework import viewsets, mixins, status
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework_extensions import mixins as emixins

from cephtool.serializers import UserSerializer

from kubernetes import client, config
from kubernetes.client.rest import ApiException
from rest_framework_extensions.mixins import NestedViewSetMixin
import pusher
import threading
import time

class PVCViewset(
    NestedViewSetMixin,
    viewsets.ViewSet,
    mixins.RetrieveModelMixin,
):

    KUBECONF = os.getenv('KUBECONF', '~/.kube/config')
    PROVISIONER = os.getenv('PROVISIONER', 'ceph.com/rbd')
    STORAGE_CLASSES = os.getenv('STORAGE_CLASSES', 'slipspace').split(',')
    kubecfg = config.load_kube_config(KUBECONF)
    apiClient = client.ApiClient(kubecfg)
    v1 = client.CoreV1Api(apiClient)
    v1_storage = client.StorageV1Api(apiClient)

    pusher_client = pusher.Pusher(
        app_id='936225',
        key='3e0d0f36638d58049acf',
        secret='ce4cd669ca5230446b72',
        cluster='us2',
        ssl=True
    )

    def sanitize(self, resource):
        return self.apiClient.sanitize_for_serialization(resource)

    @action(methods=['get'], detail=False)
    def all(self, request, *args, **kwargs):
        ns = kwargs['parent_lookup_ns']
        ret = self.v1.list_namespaced_persistent_volume_claim(ns)
        return Response(self.sanitize(ret.items))

    def retrieve(self, request, pk=None, *args, **kwargs):
        ns = kwargs['parent_lookup_ns']
        ret = self.v1.read_namespaced_persistent_volume_claim(pk, ns)
        return Response(self.sanitize(ret))

    def testfn(threadName, client, channel):
        count = 0
        while count < 100:
            time.sleep(random.uniform(0.1, 1.5))
            count += random.randint(1, 10)
            if count > 100:
                count = 100
            client.trigger(channel, 'progress', {
                'progress': count,
            })

    @action(methods=['get'], detail=True)
    def backup(self, request, pk=None, *args, **kwargs):
        ns = kwargs['parent_lookup_ns']
        pvc = self.v1.read_namespaced_persistent_volume_claim(pk, ns)
        pv = self.v1.read_persistent_volume(pvc.spec.volume_name)

        thread1 = threading.Thread(target=self.testfn, args=(self.pusher_client, pvc.metadata.uid))
        thread1.start()

        return Response(self.sanitize(pv.spec.rbd))



