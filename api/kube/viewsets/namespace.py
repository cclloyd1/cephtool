import os

from django.contrib.auth.models import User
from rest_framework import viewsets, mixins, status
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied

from cephtool.serializers import UserSerializer

from kubernetes import client, config
from kubernetes.client.rest import ApiException
from rest_framework_extensions.mixins import NestedViewSetMixin


class NamespaceViewset(
    NestedViewSetMixin,
    viewsets.ViewSet,
):

    KUBECONF = os.getenv('KUBECONF', '~/.kube/config')
    PROVISIONER = os.getenv('PROVISIONER', 'ceph.com/rbd')
    STORAGE_CLASSES = os.getenv('STORAGE_CLASSES', 'slipspace').split(',')
    kubecfg = config.load_kube_config(KUBECONF)
    apiClient = client.ApiClient(kubecfg)
    v1 = client.CoreV1Api(apiClient)
    v1_storage = client.StorageV1Api(apiClient)

    def sanitize(self, resource):
        return self.apiClient.sanitize_for_serialization(resource)

    @action(methods=['get'], detail=False)
    def all(self, request, *args, **kwargs):
        ret = self.v1.list_namespace()
        return Response(self.sanitize(ret.items))

    def retrieve(self, request, pk=None, *args, **kwargs):
        ret = self.v1.list_namespace()
        for ns in ret.items:
            if ns.metadata.name == pk:
                return Response(self.sanitize(ns))
        return Response(self.sanitize(ret.items))

