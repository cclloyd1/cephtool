from rest_framework import viewsets, mixins
from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action, permission_classes
import hashlib
from rest_framework.exceptions import PermissionDenied, ValidationError
import json

from .serializers import UserSerializer, UserPublicSerializer


class UserViewSet(
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def retrieve(self, request, *args, **kwargs):
        return Response(status=403)


    @action(methods=['get'], detail=True, permission_classes=[IsAuthenticated])
    def gravatar(self, request, pk=None):
        model = self.get_object()

        gravatar = hashlib.md5(model.email.encode("utf-8")).hexdigest()
        gravatar_url = f'https://www.gravatar.com/avatar/{gravatar}'

        return Response(gravatar_url)

    @action(methods=['get'], detail=False, permission_classes=[IsAuthenticated])
    def info(self, request):
        user = None
        if request and hasattr(request, "user"):
            user = request.user

        gravatar = hashlib.md5(user.email.encode("utf-8")).hexdigest()
        gravatar_url = "https://www.gravatar.com/avatar/%s" % gravatar
        serializer = UserSerializer(user)
        data = serializer.data
        data['gravatar'] = gravatar_url
        return Response(data)

    @action(methods=['get'], detail=True)
    def public(self, request, pk=None):
        user = self.get_object()

        gravatar = hashlib.md5(user.email.encode("utf-8")).hexdigest()
        gravatar_url = "https://www.gravatar.com/avatar/%s" % gravatar

        serializer = UserPublicSerializer(user)
        data = serializer.data
        data['gravatar'] = gravatar_url
        return Response(data)

    @action(methods=['post'], detail=False)
    def register(self, request):
        #settings = Globaloptions.objects.first()
        #if not settings.registrationEnabled:
        #    raise PermissionDenied('Registration is not enabled on this server.  To create an account, you must do so through the admin interface.')

        data = json.loads(request.body.decode('utf-8'))
        username = data['username']
        email = data['email']
        password = data['password']
        firstName = data['firstName']
        lastName = data['lastName']

        try:
            user = User.objects.create_user(username, email=email, password=password, first_name=firstName, last_name=lastName)
        except Exception as e:
            raise PermissionDenied(e)

        return Response(True)

