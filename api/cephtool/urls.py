from django.contrib import admin
from django.urls import path, re_path, include
# import api.viewsets as api_viewsets
from django.views import generic
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view
from rest_framework_extensions.routers import ExtendedSimpleRouter
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from django.conf.urls.static import static


from . import settings
import cephtool.viewsets as api_viewsets

from rbdbackup.urls import router as rbdbackup
from kube.urls import router as kube
from ceph.urls import router as ceph
from appsettings.urls import router as appsettings
from .views import index

router = ExtendedSimpleRouter()
router.register(r'^api/user', api_viewsets.UserViewSet, 'user')
urlpatterns = []

urlpatterns += [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
    re_path(r'^api-auth/', include('rest_framework.urls')),
    re_path(r'^$', generic.RedirectView.as_view(url='/api/', permanent=False)),
    re_path(r'^api/$', get_schema_view()),
    re_path(r'^api/auth/', include('rest_framework.urls', namespace='rest_framework_auth')),
    re_path(r'^api/auth/token/obtain/$', TokenObtainPairView.as_view()),
    re_path(r'^api/auth/token/refresh/$', TokenRefreshView.as_view()),
    #re_path(r'^api/auth/register/', apiviews.register, name='register'),
]


urlpatterns += router.urls
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += rbdbackup.urls
urlpatterns += kube.urls
urlpatterns += ceph.urls
urlpatterns += appsettings.urls


#handler404 = 'cephtool.views.handler404'
react_urls = [
    re_path(r'^settings/?$', index),
    re_path(r'^login/?$', index),
    re_path(r'^register/?$', index),
    # re_path(r'^(?:.*)/?$', index),

]
if not settings.DEBUG:
    urlpatterns += react_urls

# TODO:  Get urls like /favicon.ico and /manifest.json to work
