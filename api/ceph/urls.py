from rest_framework.routers import SimpleRouter
from rest_framework_extensions.routers import ExtendedSimpleRouter

from .viewsets.ceph import CephViewset
from .viewsets.pool import PoolViewset
from .viewsets.image import ImageViewset

router = ExtendedSimpleRouter()

router.register(r'^api/ceph', CephViewset, basename='ceph')

(
    router.register(r'^api/ceph/pool', PoolViewset, basename='pool')
          .register(r'image', ImageViewset, basename='image', parents_query_lookups=['pool'])
          #.register(r'pv', PVViewset, basename='pv', parents_query_lookups=['ns', 'pvc'])
)
#router.register(r'api/kubenamespace', NamespaceViewset, 'kube-ns', parents_query_lookups=['kube_ns'])

