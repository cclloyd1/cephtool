import os

from django.contrib.auth.models import User
from rest_framework import viewsets, mixins, status
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied

from rest_framework_extensions.mixins import NestedViewSetMixin

import rados
import rbd

class PoolViewset(
    NestedViewSetMixin,
    viewsets.ViewSet,
):

    conf_path = '/etc/ceph/ceph.conf'
    key_path = '/etc/ceph/ceph.client.admin.keyring'
    cluster = rados.Rados(conffile=conf_path, conf=dict(keyring=key_path))

    @action(methods=['get'], detail=False)
    def all(self, request, *args, **kwargs):
        self.cluster.connect()
        pools = self.cluster.list_pools()
        self.cluster.shutdown()
        return Response(pools)


    '''@action(methods=['get'], detail=False)
    def health(self, request, *args, **kwargs):
        self.cluster.connect()
        pools = self.cluster.list_pools()

        return Response(pools)'''

