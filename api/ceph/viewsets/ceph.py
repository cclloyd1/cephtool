import os
import subprocess

from django.contrib.auth.models import User
from rest_framework import viewsets, mixins, status
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied

from rest_framework_extensions.mixins import NestedViewSetMixin

import rados
import rbd


class CephViewset(
    NestedViewSetMixin,
    viewsets.ViewSet,
):
    conf_path = '/etc/ceph/ceph.conf'
    key_path = '/etc/ceph/ceph.client.admin.keyring'
    cluster = rados.Rados(conffile=conf_path, conf=dict(keyring=key_path))

    @action(methods=['get'], detail=False)
    def fsid(self, request, *args, **kwargs):
        self.cluster.connect()
        fsid = self.cluster.get_fsid()
        self.cluster.shutdown()
        return Response(fsid)

    @action(methods=['get'], detail=False)
    def health(self, request, *args, **kwargs):
        cmd = 'ceph health --connect-timeout 5'
        health = subprocess.getoutput(cmd)
        return Response(health)

    @action(methods=['get'], detail=False)
    def stats(self, request, *args, **kwargs):
        self.cluster.connect()
        stats = self.cluster.get_cluster_stats()
        self.cluster.shutdown()
        return Response(stats)

