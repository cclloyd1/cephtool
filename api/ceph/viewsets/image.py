import math
import os
import threading
import time
import random
from math import floor

import pusher
from django.contrib.auth.models import User
from rest_framework import viewsets, mixins, status
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied

from rest_framework_extensions.mixins import NestedViewSetMixin

import rados
import rbd

from appsettings.models import AppSettings


class ImageViewset(
    NestedViewSetMixin,
    viewsets.ViewSet,
):
    conf_path = '/etc/ceph/ceph.conf'
    key_path = '/etc/ceph/ceph.client.admin.keyring'
    cluster = rados.Rados(conffile=conf_path, conf=dict(keyring=key_path))
    cluster.connect()
    progress = 0
    pusher_client = pusher.Pusher(
        app_id='936225',
        key='3e0d0f36638d58049acf',
        secret='ce4cd669ca5230446b72',
        cluster='us2',
        ssl=True
    )
    done = False

    @action(methods=['get'], detail=False)
    def all(self, request, pk=None, *args, **kwargs):
        pool = kwargs['parent_lookup_pool']

        # Open pool and get images
        ioctx = self.cluster.open_ioctx(pool)
        rbd_inst = rbd.RBD()
        images = rbd_inst.list(ioctx)
        ioctx.close()
        return Response(images)

    def measure_progress(self, client, image_name):
        current = 0
        while current < 100 and not self.done:
            if self.progress != current:
                current = self.progress
                client.trigger(image_name, 'progress', {
                    'progress': self.progress,
                })

    def export_image(self, client, pool, image_name):
        thread1 = threading.Thread(target=self.measure_progress, args=(self.pusher_client, image_name))
        thread1.start()

        # Open pool and get images
        ioctx = self.cluster.open_ioctx(pool)
        image = rbd.Image(ioctx, image_name)
        size = image.size()  # Size is Bytes.  1 GiB for test image.


        settings = AppSettings.objects.first()
        chunk_size = settings.memoryRead * 1024 * 1024
        # TODO:  Change backup dir to actual destionation
        BACKUP_DIR = os.path.abspath('/pycharm')

        with open(os.path.join(BACKUP_DIR, 'test.img'), 'wb') as f:
            for pos in range(0, math.ceil(size/chunk_size)):
                length = chunk_size
                offset = min(pos * chunk_size, size)
                if pos == math.ceil(size/chunk_size)-1:
                    length = size - (pos)*chunk_size
                # print(f'Start: {min(pos * chunk_size, size)}  /  Length: {length}')
                # print(f'End:   {min(pos * chunk_size, size)+length}')
                # TODO:  Write to file here.  Create RBD backup model here.
                #   In RBDBackup model, write txt file in backup location containing info on .img file
                #      such as PVC name, PV name, size, etc.
                # binary = image.read(0, image.size())
                f.write(image.read(offset, length))
                self.progress = (pos+1)/math.ceil(size/chunk_size)
                print(f'Done with chunk for {image_name}')
        print(f'Done with image {image_name}')
        self.done = True

        ioctx.close()
        return True

    @action(methods=['get'], detail=True)
    def size(self, request, pk=None, *args, **kwargs):
        pool = kwargs['parent_lookup_pool']

        ioctx = self.cluster.open_ioctx(pool)
        image = rbd.Image(ioctx, pk)
        size = image.size()

        image.close()
        ioctx.close()
        # Size is bytes
        return Response(size)

    @action(methods=['get'], detail=True)
    def backup(self, request, pk=None, *args, **kwargs):
        pool = kwargs['parent_lookup_pool']

        thread1 = threading.Thread(target=self.export_image, args=(self.pusher_client, pool, pk))
        thread1.start()

        return Response(True)

