FROM python:3.7-slim

# Add user
ARG APP_USER=abc
RUN groupadd -r ${APP_USER} && useradd --no-log-init -r -g ${APP_USER} ${APP_USER}

# Install dependencies
ADD api/requirements.txt /app/
RUN set -ex \
    && BUILD_DEPS=" \
        postgresql-client \
        libsasl2-dev \
        python-dev \
        libldap2-dev \
        libssl-dev \
        gcc \
    " \
    && apt-get update && apt-get install -y --no-install-recommends $BUILD_DEPS \
    && pip install --no-cache-dir -r /app/requirements.txt \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false  $BUILD_DEPS \
    && apt-get install -y --no-install-recommends libldap2-dev nginx postgresql-client \
    && rm -rf /var/lib/apt/lists/*

# Set uWSGI settings
ENV UWSGI_WSGI_FILE=/app/api/cephtool/wsgi.py UWSGI_HTTP=:8000 UWSGI_MASTER=1 UWSGI_HTTP_AUTO_CHUNKED=1 UWSGI_HTTP_KEEPALIVE=1 UWSGI_LAZY_APPS=1 UWSGI_WSGI_ENV_BEHAVIOR=holy PYTHONUNBUFFERED=1 UWSGI_WORKERS=2 UWSGI_THREADS=4
ENV UWSGI_STATIC_EXPIRES_URI="/static/.*\.[a-f0-9]{12,}\.(css|js|png|jpg|jpeg|gif|ico|woff|ttf|otf|svg|scss|map|txt) 315360000"
ENV PYTHONPATH=$PYTHONPATH:/app/api

# Set docker settings
WORKDIR /app
EXPOSE 80
ENV DB_PORT=5432 DB_NAME=cephtool DB_USER=cephtool DB_HOST=localhost

# Set entrypoint
ADD entrypoint.sh /
RUN chmod 755 /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

# Copy app files
ADD api /app/api
ADD http /app/http/cephtool
ADD nginx.conf /etc/nginx/nginx.conf

# Collect static files
RUN mkdir /app/staticfiles && python /app/api/manage.py collectstatic --noinput


# Set user to run image as (default 1000)
# uWSGI running as root, you can use --uid/--gid/--chroot options (?Is this needed?)
#USER ${APP_USER}:${APP_USER}
